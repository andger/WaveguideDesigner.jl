# WaveguideDesigner

## About

`WaveguideDesigner.jl` is a package calculating the waveguide refractive
index structure that will support a mode of a specified field.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/WaveguideDesigner.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for WaveguideDesigner.jl](https://pawelstrzebonski.gitlab.io/WaveguideDesigner.jl/).
