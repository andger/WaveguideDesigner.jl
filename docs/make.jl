using Documenter
import WaveguideDesigner

makedocs(
    sitename = "WaveguideDesigner",
    pages = [
        "Home" => "index.md",
        "Examples and Usage" => ["Basic Usage" => "example.md"],
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => ["mode2waveguide.jl" => "mode2waveguide.md"],
        "test/" => ["mode2waveguide.jl" => "mode2waveguide_test.md"],
    ],
)
