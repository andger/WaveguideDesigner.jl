# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Implement support for 2D modes and structures
* Better derivation of practical approximate index structures
* Add imposition of design constraints (especially minimal size constraints)
* Incorporate constrained optimization routines for structure design

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to provide utilities for deriving more practical approximate index structure solutions
* We aim to produce output appropriate for use with the [`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl) package
* We aim to minimize the amount of code and function repetition when implementing the above features
