# Example Usage

As a simple example, let's start by trying to find a waveguide structure
that supports a super-Gaussian mode. We start by calculating the modal
field form:

```@example 1d
import WaveguideDesigner
import Plots: plot
import Plots: savefig # hide

lambda0 = 1
dx = 0.01
x = -2:dx:2
U = @. exp(-x^8)

plot(x, U,
	xlabel="x [Wavelengths]",
	ylabel="Modal Field",
	legend=false,
)
savefig("field.png"); nothing # hide
```

![Desired Super-Gaussian Modal Field](field.png)

Having calculated the desired modal field, we can calculate the index
profile that will support this field by directly solving the scalar
Helmholtz waveguide equation. However, the solution is not unique so
we provide a desired modal effective index value to obtain a single
solution (in this case 3, for a semi-reasonable semiconductor refractive
index value):

```@example 1d
# Solve for the waveguide index structure
(dx2, n) = WaveguideDesigner.waveguide_from_field(dx, U, lambda0,
	neff = 3)

# Plot the results
x2=cumsum(dx2); x2.-=x2[end]/2
plot(
	plot(x2, real.(n),
		ylabel="Real Refractive Index",
	),
	plot(x2, imag.(n),
		ylabel="Imaginary Refractive Index",
	),
	xlabel="x [Wavelengths]",
	legend=false,
)
savefig("neff3sol.png"); nothing # hide
```

![Waveguide Index Structure](neff3sol.png)

Well, this does not seem to be a particularly practical structure, especially
further away from the center. However, as most of the "unreasonable" bits
are well into the evanescent tail of the mode then we may be able simply
replace those with a more convenient cladding structure and still get
an acceptable approximation of the desired mode form.

Instead of providing the modal effective index, we can instead provide
a lower and upper bound on the waveguide index structure values. Doing
so can help obtain more practical structures but comes at the cost of
transverse scaling (so rather than waveguide a few
wavelengths wide we may obtain one much greater or smaller that supports
a similarly greater or smaller modal field):

```@example 1d
# Solve for the waveguide index structure
(dx2, n) = WaveguideDesigner.waveguide_from_field(dx, U, lambda0,
	nmin = 3.4, nmax = 3.5, isbounded = true)

# Plot the results
x2=cumsum(dx2); x2.-=x2[end]/2
plot(
	plot(x2, real.(n),
		ylabel="Real Refractive Index",
	),
	plot(x2, imag.(n),
		ylabel="Imaginary Refractive Index",
	),
	xlabel="x [Wavelengths]",
	legend=false,
)
savefig("nmin34nmax35sol.png"); nothing # hide
```

![Waveguide Index Structure](nmin34nmax35sol.png)

Indeed, the transverse scaling in this particular case is quite severe,
but we find that the resulting index structure is purely real and bounded
between the desired index values.

Continuously varying index structures can be difficult to fabricate/implement,
so it may be desirable to "binarize" in index structure to only take on
a pair of values. This will snap the calculated index values to either
`nmin` or `nmax` (whichever is closest). This is very much an approximation,
so the mode of the resulting structure may not be anything close to what
you want it to be.

```@example 1d
# Solve for the waveguide index structure
(dx2, n) = WaveguideDesigner.waveguide_from_field(dx, U, lambda0,
	nmin = 3.4, nmax = 3.5, isbounded = true, discretize = true)

# Plot the results
x2=cumsum(dx2); x2.-=x2[end]/2
plot(
	plot(x2, real.(n),
		ylabel="Real Refractive Index",
	),
	plot(x2, imag.(n),
		ylabel="Imaginary Refractive Index",
	),
	xlabel="x [Wavelengths]",
	legend=false,
)
savefig("nmin34nmax35solbin.png"); nothing # hide
```

![Waveguide Index Structure](nmin34nmax35solbin.png)
