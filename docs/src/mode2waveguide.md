# Calculate Waveguide from Mode

Use the scalar Helmholtz equation to solve for the index structure
given a modal field. Apply various constraints/filters to try to get more
reasonable approximate solutions.

```@autodocs
Modules = [WaveguideDesigner]
Pages   = ["mode2waveguide.jl"]
```
