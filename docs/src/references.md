# References

The method for directly designing dielectric waveguides from the desired modal
field is based on the solution of the scalar Helmholtz waveguide equation,
and it is derived/discussed in one of my conference papers, 
["Direct Semiconductor Diode Laser Mode Engineering and Waveguide Design"](https://doi.org/10.1109/ipcon.2019.8908423),
and my MS thesis,
["Semiconductor Laser Mode Engineering via Waveguide Index Structuring"](http://hdl.handle.net/2142/102511).
The output index structures from this package should be appropriate for
use/verification using the 
[`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl)
finite difference eigenmode solving package.

Direct solution of the Helmholtz waveguide equation has been previously
used in designing optical fiber,
["The Airy fiber: an optical fiber that guides light diffracted by a circular aperture"](https://doi.org/10.1364/OPTICA.3.000270),
as well as determining the index structure given modal profile measurements,
["Index distribution of optical waveguides from their mode profile"](https://doi.org/10.1109/JLT.1983.1072065).
