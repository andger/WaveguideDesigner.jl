# TODO: Citations and explanations 

"""
    cdiff(U, x)

First order central difference dU/dx.
"""
function cdiff(U::AbstractVector, x::AbstractVector)
    @assert size(U) == size(x)
    U2 = [2 * U[1] - U[2]; U; 2 * U[end] - U[end-1]]
    x2 = [2 * x[1] - x[2]; x; 2 * x[end] - x[end-1]]
    (U2[3:end] .- U2[1:end-2]) ./ (x2[3:end] .- x2[1:end-2])
end

"""
    cdiff2(U, x)

Second order central difference d^2U/dx^2.
"""
function cdiff2(U::AbstractVector, x::AbstractVector)
    cdiff(cdiff(U, x), x)
end

"""
    mode2waveguide(dx, U, lambda0, nmin, nmax)->(dx, n)

Given modal field `U` sampled with grid spacing `dx` at wavelength
`lambda0` calculate the supporting waveguide refractive index structure
`n` sampled with grid spacing `dx`. Will transversely scale the field
to obtain waveguide index structure bounded between `nmin` and `nmax`.
"""
function mode2waveguide(dx, U::AbstractVector, lambda0::Number, nmin::Number, nmax::Number)
    dx = iszero(ndims(dx)) ? fill(dx, length(U)) : dx
    k0 = 2 * pi / lambda0
    neff, w = boundsparams(dx, U, k0, nmin, nmax)
    dx ./ w, mode2waveguide(dx ./ w, U, lambda0, neff)[2]
end
"""
    mode2waveguide(dx, U, lambda0, neff)->(dx, n)

Given modal field `U` sampled with grid spacing `dx` at wavelength
`lambda0` calculate the supporting waveguide refractive index structure
`n` sampled with grid spacing `dx`. Will ensure that the supported mode
will have a modal effective index of `neff`.
"""
function mode2waveguide(dx, U::AbstractVector, lambda0::Number, neff::Number)
    dx = iszero(ndims(dx)) ? fill(dx, length(U)) : dx
    @assert size(dx) == size(U)
    k0 = 2 * pi / lambda0
    x = cumsum(dx)
    dx, sqrt.(Complex.(neff^2 .- cdiff2(U, x) ./ (k0^2 * U)))
end

"""
    boundsparams(dx, U, k0, nmin, nmax)->(neff, w)

Determine `neff` and `w` such that we can construct a waveguide index
structure bounded by `nmin` and `nmax`.
"""
function boundsparams(
    dx::AbstractVector,
    U::AbstractVector,
    k0::Number,
    nmin::Number,
    nmax::Number,
)
    #Xmin, Xmax=extrema(cdiff2(U, cumsum(dx))./(k0^2*U))
    Xmin, Xmax = extrema(cdiff2(U, cumsum(dx)) ./ (k0^2 * U))
    neff = sqrt((nmax^2 * Xmax - nmin^2 * Xmin) / (Xmax - Xmin))
    w = sqrt((nmax^2 - nmin^2) / (Xmax - Xmin))
    neff, w
end

"""
    waveguide_from_field(dx, U, lambda0; neff = 0, nmin = 0, nmax = 0, isbounded = false, discretize = false) -> (dx, n)

Calculate the waveguide index structure (`n` with sample spacing `dx`)
that will support a modal field `U` sampled with grid spacing `dx`
at wavelength `lambda0`. If `isbounded` is `false`, then waveguide
will be made to support the modal field with a modal effective index
of `neff`. If `isbounded` is `true` then it will transversely stretch
the field to generate a waveguide index structure bounded between
`nmin` and `nmax`. If `discretize` is `true`, then each point in the obtained index
structure will be snapped to the nearest of `nmin` and `nmax`.
"""
function waveguide_from_field(
    dx,
    U::AbstractVector,
    lambda0::Number;
    neff::Number = 0,
    nmin::Number = 0,
    nmax::Number = 0,
    isbounded::Bool = false,
    discretize::Bool = false,
)
    dxwvg, nwvg = isbounded ? mode2waveguide(dx, U, lambda0, nmin, nmax) :
        mode2waveguide(dx, U, lambda0, neff)
    if discretize
        nwvg = snapto(nwvg, [nmin, nmax])
    end
    dxwvg, nwvg
end

"""
    snapto(x, vals)

Convert all values in `x` to the closest value in the list `vals`.
"""
snapto(x::AbstractVector, vals::AbstractVector) =
    (x -> vals[findmin(abs.(vals .- x))[2]]).(x)

"""
    n2w(n, dx)->(warr, narr)

Given discretized arrays `n` and `dx` for index and sampling spacing,
reconstruct the corresponding arrays for index band widths and values,
`warr` and `narr`.
"""
function n2w(n::AbstractVector, dx)
    dx = iszero(ndims(dx)) ? fill(dx, length(n)) : dx
    @assert size(n) == size(dx)
    narr = [n[1]]
    warr = [dx[1]]
    for i = 2:length(n)
        if n[i] == n[i-1]
            warr[end] += dx[i]
        else
            append!(narr, n[i])
            append!(warr, dx[i])
        end
    end
    warr, narr
end
