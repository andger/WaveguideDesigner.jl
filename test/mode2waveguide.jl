x = 0:10
@test WaveguideDesigner.cdiff(x, x) == ones(length(x))
@test WaveguideDesigner.cdiff2(x, x) == zeros(length(x))

x = 0:10
x2 = x .^ 2
@test WaveguideDesigner.cdiff(x2, x)[2:end-1] == 2 .* x[2:end-1]
@test WaveguideDesigner.cdiff2(x2, x)[3:end-2] == 2 .* ones(length(x[3:end-2]))

@test WaveguideDesigner.snapto(x, [3, 8]) == [3, 3, 3, 3, 3, 3, 8, 8, 8, 8, 8]

n = [1, 1, 1, 2, 2, 3, 3, 3, 3]
dx = ones(length(n))
@test WaveguideDesigner.n2w(n, dx) == ([3, 2, 4], [1, 2, 3])

# Run basic sanity tests on input/output to waveguide generator function
#TODO: better test?

dx = 0.1
x = -5:dx:5
dxv = fill(dx, length(x))
U = @. exp(-x^4)
lambda0 = 1

(dx2, n) = WaveguideDesigner.waveguide_from_field(dx, U, lambda0; neff = 3)
@test size(n) == size(dx2)
(dx2, n) = WaveguideDesigner.waveguide_from_field(dxv, U, lambda0; neff = 3)
@test size(n) == size(dx2)
(dx2, n) = WaveguideDesigner.waveguide_from_field(
    dx,
    U,
    lambda0;
    nmin = 2,
    nmax = 3,
    isbounded = true,
)
@test size(n) == size(dx2)
@test all(2 .<= real.(n) .<= 3)
(dx2, n) = WaveguideDesigner.waveguide_from_field(
    dx,
    U,
    lambda0;
    nmin = 2,
    nmax = 3,
    isbounded = true,
    discretize = true,
)
@test size(n) == size(dx2)
@test unique(n) == [2, 3]
